﻿public enum gameStateEnum
{
    GamePending,
    GameStarted,
    GameFinished,
}