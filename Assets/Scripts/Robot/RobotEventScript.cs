﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class RobotEventScript : MonoBehaviour
{
    public int robotId;

    //  Statistiques test
    public int testStatAttack;
    public int testStatHp;
    public int testStatSpeed;

    public int testBehaviorProximity;
    public int testBehaviorAgility;
    public int testBehaviorAggressivity;

    public int testBulletId;
    public int testBulletSpeed;
    public int testMaxRange;
    public int testMinRange;
    public int testRateOfFire;
    public int testDamageValue;

    // Robot Controllers
    [SerializeField] private GameManagerScript gameManager;
    private RobotHUDController HUDController;
    private DroneBot_Actions animationController;
    public RobotDBController dBController;

    //  Robot Variables
    [SerializeField] Transform robotBulletSpawn;
    public Robot robot;
    private Rigidbody robotRigidbody;
    public RobotWeapon robotWeapon;

    //  Enemy Variables
    [SerializeField] GameObject enemyGameObject;
    public Robot enemy;
    public RobotWeapon enemyWeapon;

    //  Robot HUD
    public Image healthDisplay;
    public Text winRateDisplay;
    public Text totalGamesDisplay;

    //  Bullets List
    [SerializeField] private GameObject[] BulletList;

    [HideInInspector] public robotGameStateEnum robotGameState;
    private robotAnimStateEnum robotStateAnim;


    private void Awake()
    {
        //  Getting Robot components
        robotRigidbody = gameObject.GetComponent<Rigidbody>();
        animationController = GetRobotAnimationController();
        HUDController = new RobotHUDController();
        robot = new Robot();
        dBController = new RobotDBController(gameManager.GetComponent<DBManagerScript>());
        robotWeapon = new RobotWeapon();

        //  Setting Robot states
        robotGameState = robotGameStateEnum.Born;
        robotStateAnim = robotAnimStateEnum.Idle;

        //  Getting Enemy Robot components
        enemy = enemyGameObject.GetComponent<RobotEventScript>().robot;
        enemyWeapon = enemyGameObject.GetComponent<RobotEventScript>().robotWeapon;


        // Checking if we want to reset winrates
        if (gameManager.gameType.Equals(gameTypeEnum.GameAlpha))
        {
            if (gameManager.resetWinrates)
            {
                dBController.RobotResetWinrate(robotId, dBController);
            }
            else
            {
                dBController.resetRobotWinrateReturn = true;
            }
        }
    }

    private void FixedUpdate()
    {
        //  Robot Setup
        if (robotGameState == robotGameStateEnum.Born)
        {
            int[] loc_robotInstanceData;
            int[] loc_weaponInstanceData;
            int[] loc_winRateInstanceData;

            if (gameManager.gameType.Equals(gameTypeEnum.GameAlpha))
            {
                if (dBController.resetRobotWinrateReturn == true)
                {
                    dBController.RobotSetupFromDB(robotId, dBController);
                    dBController.resetRobotWinrateReturn = false;
                }

                if (dBController.getRobotDataReturn && dBController.getWeaponDataFromRobotIdReturn && dBController.getRobotWinrateReturn)
                {
                    loc_robotInstanceData = dBController.returnRobotData;
                    loc_weaponInstanceData = dBController.returnWeaponDataFromRobotId;
                    loc_winRateInstanceData = dBController.returnRobotWinrate;

                    RobotStatController.StatSetup(robot, robotWeapon, loc_robotInstanceData, loc_weaponInstanceData);

                    robotWeapon.bullet = BulletList[robotWeapon.bulletId];
                    HUDController.updateWinRateDisplay(winRateDisplay, totalGamesDisplay, loc_winRateInstanceData);

                    /*
                        enemy = enemyGameObject.GetComponent<RobotEventScript>().robot;
                        enemyWeapon = enemyGameObject.GetComponent<RobotEventScript>().robotWeapon;
                        Debug.Log("enemy attack: " + enemy.statAttack);
                    */

                    dBController.returnRobotData = null;
                    dBController.returnWeaponDataFromRobotId = null;
                    dBController.getRobotDataReturn = false;
                    dBController.getWeaponDataFromRobotIdReturn = false;
                    dBController.getRobotWinrateReturn = false;

                    robotGameState = robotGameStateEnum.Ready;
                }
            }

            if (gameManager.gameType.Equals(gameTypeEnum.GameTest))
            {
                //robot = new Robot(testStatAttack, testStatHp, testStatSpeed, testBehaviorProximity, testBehaviorAgility, testBehaviorAggressivity);
                //robotWeapon = new RobotWeapon(testBulletId, testBulletSpeed, testMaxRange, testMinRange, testRateOfFire, testDamageValue);

                loc_robotInstanceData = new int[] { testStatAttack, testStatHp, testStatSpeed, testBehaviorProximity, testBehaviorAgility, testBehaviorAggressivity };
                loc_weaponInstanceData = new int[] { testBulletId, testBulletSpeed, testMaxRange, testMinRange, testRateOfFire, testDamageValue };

                RobotStatController.StatSetup(robot, robotWeapon, loc_robotInstanceData, loc_weaponInstanceData);

                robotWeapon.nextFire = 1 / robotWeapon.rateOfFire;
                robotWeapon.readyToShoot = false;

                robotGameState = robotGameStateEnum.Ready;
            }
        }

        //  Robot actions
        if (gameManager.gameState == gameStateEnum.GameStarted)
        {
            if (robotRigidbody.velocity.magnitude > 10)
            {
                NormalizeRigidBodyVelocity();
            }

            robot.RobotAim(transform, enemyGameObject.transform.position);

            if (robot.RobotCheckAngle(transform, enemyGameObject.transform.position))
            {
                if (robot.RobotCheckRange(transform, enemyGameObject.transform.position) >= robotWeapon.maxRange)
                {
                    robot.RobotMove(1, robotRigidbody);
                    robot.RobotDodge(transform, robotRigidbody);
                }
                else if (robot.RobotCheckRange(transform, enemyGameObject.transform.position) <= robotWeapon.minRange)
                {
                    robot.RobotMove(-1, robotRigidbody);
                    robot.RobotDodge(transform, robotRigidbody);
                }
                else
                {
                    RobotEventShoot();
                }
            }

            if (robot.curentStatHp <= 0)
            {
                AnimationStateChecker(robotAnimStateEnum.Dead);
                robotRigidbody.isKinematic = true;
                enemyGameObject.GetComponent<Rigidbody>().isKinematic = true;
                robotGameState = robotGameStateEnum.Dead;
            }
        }

        
        if (enemyGameObject.GetComponent<RobotEventScript>().robotGameState == robotGameStateEnum.Dead)
        {
            //AnimationStateChecker(robotAnimStateEnum.Idle);
            animationController.Idle1();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (gameManager.gameState == gameStateEnum.GameStarted)
        {
            //Robot hits bullet
            if (collision.gameObject.tag == "BulletTag")
            {
                DroneBot_Shell bulletCollided;
                bulletCollided = collision.gameObject.GetComponent<DroneBot_Shell>();

                if (bulletCollided.robotBulletId != robotId)
                {
                    animationController.Hit1();
                    robot.curentStatHp -= enemy.statAttack / 10 * enemyWeapon.damageValue;
                    robot.curentStatHp = RobotStatController.NormalizeCurentHpValue(robot.curentStatHp);
                    HUDController.updateHealthBar(healthDisplay, robot.curentStatHp, robot.statHp);
                }
            }
        }
    }

    //  Récupération du scricpt gérant les animations
    DroneBot_Actions GetRobotAnimationController()
    {
        DroneBot_Actions loc_animationController = new DroneBot_Actions();
        Component[] loc_retrievedScripts;

        loc_retrievedScripts = GetComponentsInChildren<DroneBot_Actions>();

        foreach (DroneBot_Actions animScript in loc_retrievedScripts)
        {
            loc_animationController = animScript;
        }

        return loc_animationController;
    }

    // Fonction de management des Animations
    void AnimationStateChecker(robotAnimStateEnum arg_robotStateAnimAfter)
    {
        if (robotStateAnim != arg_robotStateAnimAfter)
        {
            if (arg_robotStateAnimAfter == robotAnimStateEnum.Idle)
            {
                animationController.Idle1();
            }

            if (arg_robotStateAnimAfter == robotAnimStateEnum.DodgeRight)
            {
                animationController.StrafeRight();
            }

            if (arg_robotStateAnimAfter == robotAnimStateEnum.DodgeLeft)
            {
                animationController.StrafeLeft();
            }

            if (arg_robotStateAnimAfter == robotAnimStateEnum.Dead)
            {
                animationController.Dead3();
            }

            robotStateAnim = arg_robotStateAnimAfter;
        }
    }

    //  Normalisation de la Vitesse du RigidBody
    void NormalizeRigidBodyVelocity()
    {
        float magnitudeDecrease = 1.5f;

        if (robotRigidbody.velocity.x > 0)
        {
            robotRigidbody.velocity = new Vector3(robotRigidbody.velocity.x - magnitudeDecrease, 0, robotRigidbody.velocity.z);
        }

        if (robotRigidbody.velocity.x < 0)
        {
            robotRigidbody.velocity = new Vector3(robotRigidbody.velocity.x + magnitudeDecrease, 0, robotRigidbody.velocity.z);
        }

        if (robotRigidbody.velocity.z > 0)
        {
            robotRigidbody.velocity = new Vector3(robotRigidbody.velocity.x, 0, robotRigidbody.velocity.z - magnitudeDecrease);
        }

        if (robotRigidbody.velocity.z < 0)
        {
            robotRigidbody.velocity = new Vector3(robotRigidbody.velocity.x, 0, robotRigidbody.velocity.z + magnitudeDecrease);
        }
    }

    //  Events d'attaque/tir
    void RobotEventShoot()
    {
        if (robotWeapon.readyToShoot)
        {
            animationController.Fire();
            //robot.RobotShoot(robotWeapon, robotBulletSpawn.position);
            robotWeapon.readyToShoot = false;
        }
        else
        {
            AnimationStateChecker(robotAnimStateEnum.Idle);
            robotWeapon.readyToShoot = robot.RobotReload(robotWeapon);
        }
    }
}
