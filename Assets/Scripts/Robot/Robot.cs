﻿using System;
using UnityEngine;

public class Robot
{
    //  Statistiques du Robot (valeurs: 0 => 100)
    public int statAttack;
    public int statHp;
    public int statSpeed;

    //  Comportements du Robot (valeurs: 0 => 100)
    public int behaviorProximity;
    public int behaviorAgility;
    public int behaviorAggressivity;

    //  Statistiques du Robot courantes (valeurs: 0 => valeur MAX)
    [HideInInspector] public int curentStatHp;


    //  Constructeur
    /*
    public Robot(int arg_attack, int arg_hp, int arg_speed, int arg_proximity, int arg_agility, int arg_aggressivity)
    {
        statAttack = arg_attack;
        statHp = arg_hp;
        statSpeed = arg_speed;

        behaviorProximity = arg_proximity;
        behaviorAgility = arg_agility;
        behaviorAggressivity = arg_aggressivity;
    }
    */

    //  Méthode de déplacement
    public void RobotMove(int arg_direction, Rigidbody arg_robotRigidbody)
    {
        double loc_robotSpeedDouble = Convert.ToSingle(arg_direction * statSpeed / 2);
        loc_robotSpeedDouble = Math.Round(loc_robotSpeedDouble, 2);
        float loc_robotSpeedFloat = (float)loc_robotSpeedDouble;

        if (statSpeed >= 0)
        {
            arg_robotRigidbody.AddForce(arg_robotRigidbody.transform.TransformDirection(new Vector3(0.0f, 0.0f, loc_robotSpeedFloat)));
        }
        else
        {
            arg_robotRigidbody.AddForce(arg_robotRigidbody.transform.TransformDirection(new Vector3(0.0f, 0.0f, 0.75f * loc_robotSpeedFloat)));
        }
    }

    //  Méthode de rotation/visée
    public void RobotAim(Transform arg_robotTransform, Vector3 arg_enemyTransformPosition)
    {
        Vector3 loc_vectorDirection = arg_enemyTransformPosition - arg_robotTransform.position;
        float loc_robotRotationStep = Convert.ToSingle(statSpeed);
        loc_robotRotationStep = (loc_robotRotationStep / 15) * Time.deltaTime;

        Vector3 loc_newDirection = Vector3.RotateTowards(arg_robotTransform.forward, loc_vectorDirection, loc_robotRotationStep, 0.0f);
        arg_robotTransform.rotation = Quaternion.LookRotation(loc_newDirection);
    }

    //  Méthode d'analyse de la distance
    public float RobotCheckRange(Transform arg_robotTransform, Vector3 arg_enemyTransformPosition)
    {
        float loc_distance = Vector3.Distance(arg_enemyTransformPosition, arg_robotTransform.position);
        return loc_distance;
    }

    //  Méthode check rotation
    public bool RobotCheckAngle(Transform arg_robotTransform, Vector3 arg_enemyTransformPosition)
    {
        Vector3 loc_targetDirection = arg_enemyTransformPosition - arg_robotTransform.position;
        float loc_angleValue = Vector3.Angle(loc_targetDirection, arg_robotTransform.forward);

        if (loc_angleValue < 45f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //  Méthode de tir
    public void RobotShoot(Transform arg_robotTransform, RobotWeapon arg_robotWeaponScript, Vector3 arg_robotBulletSpawnTransformPosition)
    {
        Vector3 loc_robotDirection = arg_robotTransform.forward;
        float loc_bulletSpeed = Convert.ToSingle(arg_robotWeaponScript.bulletSpeed) * 200;

        GameObject loc_instBullet = MonoBehaviour.Instantiate(arg_robotWeaponScript.bullet, arg_robotBulletSpawnTransformPosition, Quaternion.identity) as GameObject;
        Rigidbody loc_instBulletRigidbody = loc_instBullet.GetComponent<Rigidbody>();
        
        loc_instBulletRigidbody.AddForce(loc_robotDirection * loc_bulletSpeed);
    }

    //  Méthode de cooldown
    public bool RobotReload(RobotWeapon arg_robotWeaponScript)
    {
        arg_robotWeaponScript.nextFire -= Time.deltaTime;
        float loc_attackAggressivity = 1 + (Convert.ToSingle(behaviorAggressivity) / 75);

        if (arg_robotWeaponScript.nextFire <= 0)
        {
            arg_robotWeaponScript.nextFire = 1 / (arg_robotWeaponScript.rateOfFire * loc_attackAggressivity);
            return true;
        }
        else
        {
            return false;
        }
    }

    //  Méthode d'Esquive
    public void RobotDodge(Transform arg_robotTransform, Rigidbody arg_robotRigidbody)
    {
        int loc_randomNumber = UnityEngine.Random.Range(0, 100);
        float loc_timer = 0.0f;
        float loc_runTime = 0.1f;
        int loc_dodgeDirectionRight = 0;
        float loc_dodgeFrequency = Convert.ToSingle(behaviorAgility) / 10;
        float loc_dodgeSpeed = Convert.ToSingle(statSpeed) * 8;

        RaycastHit loc_raycastHit;

        //Debug.DrawRay(transform.position, transform.right * 3, Color.red);
        //Debug.DrawRay(transform.position, -transform.right * 3, Color.red);

        if ((loc_randomNumber % 2) == 0)
        {
            if (!Physics.Raycast(arg_robotTransform.position, arg_robotTransform.right, out loc_raycastHit, 3))
            {
                loc_dodgeDirectionRight = 1;
            }
            else
            {
                loc_dodgeDirectionRight = -1;
            }
            
        }
        else if ((loc_randomNumber % 2) != 0)
        {
            if (!Physics.Raycast(arg_robotTransform.position, -arg_robotTransform.right, out loc_raycastHit, 3))
            {
                loc_dodgeDirectionRight = -1;
            }
            else
            {
                loc_dodgeDirectionRight = 1;
            }  
        }

        if (loc_randomNumber + (loc_dodgeFrequency) > 100)
        {
            //Debug.Log("ça passe");

            while (loc_timer < loc_runTime)
            {
                arg_robotRigidbody.AddForce(arg_robotRigidbody.transform.TransformDirection(new Vector3(loc_dodgeDirectionRight * loc_dodgeSpeed, 0.0f, 0.0f)));
                loc_timer += Time.deltaTime;
            }

            arg_robotRigidbody.velocity = new Vector3(0, 0, arg_robotRigidbody.velocity.z);
        }
    }
}