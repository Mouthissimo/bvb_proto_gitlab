﻿public enum robotAnimStateEnum
{
    Idle,
    DodgeRight,
    DodgeLeft,
    Hit,
    Dead,
}