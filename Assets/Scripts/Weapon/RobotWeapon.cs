﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotWeapon
{
    public GameObject bullet;
    public int bulletId;
    public float nextFire;
    public bool readyToShoot;

    //  Statistiques de l'Arme
    public int bulletSpeed;
    public int maxRange;
    public int minRange;
    public int rateOfFire;
    public int damageValue;

    /*
    public RobotWeapon(int arg_bulletId, int arg_bulletSpeed, int arg_maxRange, int arg_minRange, int arg_rateOfFire, int arg_damageValue)
    {
        bulletId = arg_bulletId;
        bulletSpeed = arg_bulletSpeed;
        maxRange = arg_maxRange;
        minRange = arg_minRange;
        rateOfFire = arg_rateOfFire;
        damageValue = arg_damageValue;
    }
    */
}
